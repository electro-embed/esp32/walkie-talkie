#include <Arduino.h>
#include <I2S.h>

#define SCK 17
#define FS 16
#define SD 2
#define OUT_SD -1
#define IN_SD -1


void setup() {
  Serial.begin(115200);
  delay(1000);
  Serial.println("initializing I2S...");

  I2S.setAllPins(SCK, FS, SD, OUT_SD, IN_SD);

  // start I2S at 16 kHz with 32-bits per sample
  if (!I2S.begin(I2S_PHILIPS_MODE, 16000, 32)) {
    Serial.println("Failed to initialize I2S!");
    while (1); // do nothing
  }
  Serial.println("I2S initialized.");
}

void loop() {
  int sample = I2S.read();
  if ((sample == 0) || (sample == -1) ) {
    return;
  }

  // convert to 18 bit signed
  sample >>= 14; 

  Serial.println(sample);
//   sleep(100);
}
 